const mongoose = require("mongoose");

const blogSchema = new mongoose.Schema({
  blog_title: String,
  blog_short_desp: String,
  blog_image: String,
  description: String,
});

const blog = mongoose.model("blog", blogSchema);
module.exports = blog;
